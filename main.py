import discord
from discord.ext import commands
from classes.config import Options
import os


class LaufralsBot(commands.Bot):
    def __init__(self):
        self.options = Options('config.ini')
        super().__init__(command_prefix=self.options['CREDENTIALS']['prefix'], intents=self.getNewAllIntent())
        self.modules = []
        for module in os.listdir('./commands/modules/'):
            if module.endswith('.py'):
                self.modules.append(module[:-3])
        self.running = False
        #self.bot_admin = []
        #self.bot.userlists.guilds_admins = {}
        #self.bot.userlists.guilds_moderators = {}
        self.print_info()

    def run(self):
        if not self.running:
            for module in self.modules:
                try:
                    self.load_extension('commands.modules.{}'.format(module.lower()))
                except Exception as e:
                    print('Failed to load extension {}\n{}: {}'.format(module, type(e).__name__, e))
            self.running = True
            super().run(self.options['CREDENTIALS']['key'])

    def print_info(self):
        print(f"Prefix: {self.options['CREDENTIALS']['prefix']}")
        print(f"DataFile: {self.options['CREDENTIALS']['data_file']}")
        print("Modules:")
        for module in self.modules:
            print(f"\t- {module}")

    def getNewCustomIntent(self):
        intents = discord.Intents.default()
        intents.members = True
        intents.voice_states = True
        intents.guilds = True
        intents.bans = True
        intents.invites = True
        intents.reactions = True
        intents.presences = True
        return intents

    def getNewAllIntent(self):
        intents = discord.Intents.all()
        return intents

if __name__ == '__main__':
    bot = LaufralsBot()
    bot.run()