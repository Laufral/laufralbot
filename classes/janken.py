from enum import Enum
from classes.context import EmptyContext
import asyncio

class JankenPlayerCancelsException(Exception):
    def __init__(self, player):
        self.player = player

class JankenBadHandException(Exception):
    def __init__(self, player, hand):
        self.player = player
        self.hand = hand

class JankenHand(Enum):
    dismiss = 0
    rock = 1
    papper = 2
    scissors = 3

    @classmethod
    def has_playablehand(cls, hand):
        playablehands = [JankenHand.rock, JankenHand.papper, JankenHand.scissors]
        return hand in playablehands

    @classmethod
    def parsevaluefromlistindex(cls, list, list_value):
        value = list.index(list_value)
        if value > len(cls.__members__):
            raise ValueError("Unknonw value")
        return JankenHand(value)

    # TODO: look for build-in methods to do this
    @classmethod
    def name(cls, hand):
        if hand == JankenHand.dismiss:
            return 'dismiss'
        elif hand == JankenHand.rock:
            return 'rock'
        elif hand == JankenHand.papper:
            return 'papper'
        elif hand == JankenHand.scissors:
            return 'scissors'
        return None


class JankenContext(EmptyContext):
    def __init__(self, name_author, name_player):
        EmptyContext.__init__(self)
        self.name_author = name_author
        self.name_player = name_player

class Janken:
    def __init__(self, gethand_hdlr):
        self.gethand_hdlr = gethand_hdlr

    async def process_game(self, ctx):
        hand_author, hand_player = await self.gethands(ctx)
        if hand_author is None:
            raise JankenPlayerCancelsException(ctx.name_author)
        if hand_player is None:
            raise JankenPlayerCancelsException(ctx.name_player)
        if not JankenHand.has_playablehand(hand_author):
            raise JankenBadHandException(ctx.name_author, hand_player)
        if not JankenHand.has_playablehand(hand_player):
            raise JankenBadHandException(ctx.name_player, hand_player)
        winner = self.process_hands(ctx, hand_author, hand_player)
        return hand_author, hand_player, winner


    async def gethands(self, ctx):
        results = await asyncio.gather(
            self.gethand(ctx, ctx.name_author),
            self.gethand(ctx, ctx.name_player)
        )
        return results[0],  results[1]

    async def gethand(self, ctx, target):
        ctx.target = target
        if target == ctx.name_author:
            ctx.message = f'Vous avez défié {ctx.name_player} au pierre papier ciseaux, réagissez a ce post pour jouer.'
        else:
            ctx.message = f'{ctx.name_author} vous a défié au pierre papier ciseaux, réagissez a ce post pour jouer.'
        return await self.gethand_hdlr(ctx)

    def process_hands(self, ctx, hand_author, hand_player):
        # Cas ou c'est l'égalité
        if hand_author == hand_player:
            return None
        # Cas ou l'adversaire est gagnant
        elif (hand_author == JankenHand.rock and hand_player == JankenHand.papper) \
            or (hand_author == JankenHand.papper and hand_player == JankenHand.scissors)\
                or (hand_author == JankenHand.scissors and hand_player == JankenHand.rock):
            return ctx.name_player
        # Cas ou le challenger est gagnant
        else:
            return ctx.name_author