import logging

def log_command(logger, ctx, result=''):
    logger.info(f"L'utilisateur '{ctx.author.name}{ctx.author.mention}' à utilisé la commande '{ctx.command}' sur le serveur '{ctx.guild.name}':\n\tRésultat: '{result}'")

def log_command_error(logger, ctx, message='', exception=''):
    logger.info(f"L'utilisateur '{ctx.author.name}{ctx.author.mention}' n'à pas pu utiliser la commande '{ctx.command}' sur le serveur '{ctx.guild.name}':\n\tCause: '{message}'\n\tException: {str(exception)}")

def log_event(logger, event='Evenement inconnu', author='Autheur inconnu', infos = "Pas d'informations"):
    logger.info(f"L'event '{event}' a été enregistré par l'autheur '{author}':\n\t{infos}")

def get_newfilelogger(loggername, loggerfilename, level=logging.INFO, console=True):
    logger = logging.getLogger(loggername)
    logger.setLevel(level)

    file_handler = get_newfilehandler(loggerfilename, level)
    logger.addHandler(file_handler)

    if console == True:
        console_handler = logging.StreamHandler()
        console_handler.setLevel(level)
        console_handler.setFormatter(get_newformatter())
        logger.addHandler(console_handler)

    return logger

def get_newformatter():
    return logging.Formatter('[%(name)s] %(asctime)s: %(levelname)s\n\t%(message)s')

def get_newfilehandler(filename, level, formatter=get_newformatter()):
    file_handler = logging.FileHandler(filename)
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)
    return file_handler

def close_logger(logger):
    handlers = logger.handlers[:]
    for handler in handlers:
        handler.close()
        logger.removeHandler(handler)
