from discord.ext import commands
import discord
import logging, common
from common import logger

class EventLoggerCog(commands.Cog):
    """Gère les événements"""

    def __init__(self, bot):
        self.bot = bot
        fh = common.get_newfilehandler(self.bot.options['LOGGERS']['event_filename'],
                                       logging.INFO,
                                       common.get_newformatter())
        self.logger_info = common.get_newfilelogger(self.bot.options['LOGGERS']['eventinfo_logname'],
                                                    self.bot.options['LOGGERS']['eventinfo_filename'],
                                                    level=logging.INFO,
                                                    console=True)
        self.logger_info.addHandler(fh)
        self.logger_error = common.get_newfilelogger(self.bot.options['LOGGERS']['eventerror_logname'],
                                                     self.bot.options['LOGGERS']['eventerror_filename'],
                                                     level=logging.INFO,
                                                     console=True)
        self.logger_error.addHandler(fh)
        """self.logger_warning = common.get_newfilelogger(self.bot.options['LOGGERS']['eventwarning_logname'],
                                                       self.bot.options['LOGGERS']['eventwarning_filename'],
                                                       level=logging.INFO,
                                                       console=True)
        self.logger_warning.addHandler(fh)"""
        self.logger_info.info("Instanciation du Cog 'EventLoggerCog'")

    def cog_unload(self):
        self.logger_info.info("Fermeture du Cog 'EventLoggerCog'")
        common.close_logger(self.logger_info)
        common.close_logger(self.logger_error)

    @commands.Cog.listener()
    async def on_connect(self):
       common.log_event(self.logger_info, 'on_connect')

    @commands.Cog.listener()
    async def on_disconnect(self):
       common.log_event(self.logger_info, 'on_connect')

    @commands.Cog.listener()
    async def on_ready(self):
       common.log_event(self.logger_info, 'on_connect')

    @commands.Cog.listener()
    async def on_resume(self):
       common.log_event(self.logger_info, 'on_resume')

    @commands.Cog.listener()
    async def on_error(self):
       common.log_event(self.logger_error, 'on_error')

    """
    @commands.Cog.listener()
    async def on_socket_raw_receive(self, message):
       common.log_event(self.logger_info, 'on_socket_raw_receive', infos=message)

    @commands.Cog.listener()
    async def on_socket_raw_send(self, payload):
       common.log_event(self.logger_info, 'on_socket_raw_send', infos=payload)
    """

    @commands.Cog.listener()
    async def on_message(self, message):
       common.log_event(self.logger_info, 'on_message', author=message.author.display_name, infos=f"Message: {message.content}\n\t{message}")

    @commands.Cog.listener()
    async def on_message_delete(self, message):
       common.log_event(self.logger_info, 'on_message_delete', author=message.author.display_name, infos=f"Message: {message.content}\n\t{message}")

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
       common.log_event(self.logger_info, 'on_message_edit', author=before.author.display_name,
                        infos=f"Before: {before}\n\tAfter: {after}")

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
       common.log_event(self.logger_info, 'on_reaction_add', author=user.mention,
                        infos=f"Message ({reaction.message.author.mention}: {reaction.message}\n"
                              f"\tEmoji: {reaction.emoji} (Count: {reaction.count}")

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction, user):
       common.log_event(self.logger_info, 'on_reaction_remove', author=user.mention,
                        infos=f"Message ({reaction.message.author.mention}: {reaction.message}\n"
                              f"\tEmoji: {reaction.emoji} (Count: {reaction.count}")

    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel):
        common.log_event(self.logger_info, 'on_guild_channel_create', author=channel.guild.owner.mension,
                         infos=f"Channel Name: {channel.name}")

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        common.log_event(self.logger_info, 'on_guild_channel_delete', author=channel.guild.owner.mension,
                         infos=f"Channel Name: {channel.name}")

    @commands.Cog.listener()
    async def on_guild_channel_update(self, before, after):
        common.log_event(self.logger_info, 'on_guild_channel_update', author=before.guild.owner.mension,
                         infos=f"Channel Name before: {before.name}\n\tChannel Name after: {after.name}")

    @commands.Cog.listener()
    async def on_guild_channel_pins_update(self, channel, last_pin):
        common.log_event(self.logger_info, 'on_guild_channel_pins_update', author=channel.guild.owner.mension,
                         infos=f"Channel Name: {channel.name}\n\tLastPin datetime: {last_pin}")

    @commands.Cog.listener()
    async def on_webhooks_update(self, channel):
        common.log_event(self.logger_info, 'on_webhooks_update', author=channel.guild.owner.mension,
                         infos=f"Channel Name: {channel.name}")

    @commands.Cog.listener()
    async def on_member_join(self, member):
        common.log_event(self.logger_info, 'on_member_join', author=member.name,
                         infos=f"Member: {member.name} joined {member.guild.name}")

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        common.log_event(self.logger_info, 'on_member_remove', author=member.name,
                         infos=f"Member: {member.name} leaved  {member.guild.name}")


    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        common.log_event(self.logger_info, 'on_member_update', author=before.name,
                         infos=f"Member before: {before}\n\tMember after: {after}")

    @commands.Cog.listener()
    async def on_user_update(self, before, after):
        common.log_event(self.logger_info, 'on_user_update', author=before.name,
                         infos=f"User before: {before}\n\tUser after: {after}")

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        common.log_event(self.logger_info, 'on_guild_join', author=guild.owner.mention,
                         infos=f"I joined or created: {guild}")

    @commands.Cog.listener()
    async def on_guild_remove(self, guild):
        common.log_event(self.logger_info, 'on_guild_remove', author=guild.owner.mention,
                         infos=f"I left, got kicked, got banned or deleted: {guild}")

    @commands.Cog.listener()
    async def on_guild_update(self, before, after):
        common.log_event(self.logger_info, 'on_guild_update', author=before.owner.mention,
                         infos=f"Guild before: {before}\n\tGuild after: {after}")

    @commands.Cog.listener()
    async def on_guild_role_create(self, role):
        common.log_event(self.logger_info, 'on_guild_role_create', author=role.guild.owner.mention,
                         infos=f"Role created: {role}")

    @commands.Cog.listener()
    async def on_guild_role_delete(self, role):
        common.log_event(self.logger_info, 'on_guild_role_delete', author=role.guild.owner.mention,
                         infos=f"Role deleted: {role}")

    @commands.Cog.listener()
    async def on_guild_role_update(self, before, after):
        common.log_event(self.logger_info, 'on_guild_role_update', author=before.guild.owner.mention,
                         infos=f"Role before: {before}\n\tRole after: {after}")

    @commands.Cog.listener()
    async def on_guild_available(self, guild):
        common.log_event(self.logger_info, 'on_guild_available', author=guild.owner.mention,
                         infos=f"Guild: {guild}")

    @commands.Cog.listener()
    async def on_guild_unavailable(self, guild):
        common.log_event(self.logger_info, 'on_guild_unavailable', author=guild.owner.mention,
                         infos=f"Guild: {guild}")

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        common.log_event(self.logger_info, 'on_voice_state_update', author=member.mention,
                         infos=f"Member: {member}\n\tVoice State before: {before}\n\tVoice State after: {after}")

    @commands.Cog.listener()
    async def on_member_ban(self, guild, user):
        common.log_event(self.logger_info, 'on_member_ban', author=guild.owner.mention,
                         infos=f"User: {user}\n\tGuild: {guild}")

    @commands.Cog.listener()
    async def on_member_unban(self, guild, user):
        common.log_event(self.logger_info, 'on_member_unban', author=guild.owner.mention,
                         infos=f"User: {user}\n\tGuild: {guild}")

    @commands.Cog.listener()
    async def on_invite_create(self, invite):
        common.log_event(self.logger_info, 'on_invite_create', author=invite.guild.owner.mention,
                         infos=f"Invitation: {invite}")

    @commands.Cog.listener()
    async def on_invite_delete(self, invite):
        common.log_event(self.logger_info, 'on_invite_delete', author=invite.guild.owner.mention,
                         infos=f"Invitation: {invite}")

    @commands.Cog.listener()
    async def on_group_join(self, channel, user):
        common.log_event(self.logger_info, 'on_group_join', author=channel.owner.mention,
                         infos=f"Channel: {channel}\n\tUser: {user}")

    @commands.Cog.listener()
    async def on_group_remove(self, channel, user):
        common.log_event(self.logger_info, 'on_group_remove', author=channel.owner.mention,
                         infos=f"Channel: {channel}\n\tUser: {user}")

    @commands.Cog.listener()
    async def on_relationship_add(self, relationship):
        common.log_event(self.logger_info, 'on_relationship_add', author=relationship.user,
                         infos=f"Relationship: {relationship}")

    @commands.Cog.listener()
    async def on_relationship_remove(self, relationship):
        common.log_event(self.logger_info, 'on_relationship_remove', author=relationship.user,
                         infos=f"Relationship: {relationship}")

    @commands.Cog.listener()
    async def on_relationship_update(self, before, after):
        common.log_event(self.logger_info, 'on_relationship_update', author=before.user,
                         infos=f"Relationship before: {before}\n\tRelationship after: {after}")

    @commands.Cog.listener()
    async def on_command(self, ctx):
        common.log_event(self.logger_info, 'on_command', author=ctx.author.mention, infos="Commande utilisée: " + ctx.command.name)

    @commands.Cog.listener()
    async def on_command_completion(self, ctx):
        common.log_event(self.logger_info, 'on_command_completion', author=ctx.author.mention, infos="Commande utilisée: " + ctx.command.name)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error=None):
        if ctx.command:
            common.log_event(self.logger_error, 'on_command_error', author=ctx.author.mention,
                         infos=f"Commande utilisée: {ctx.command.name}\n\tErreur: {error}")
        else:
            common.log_event(self.logger_error, 'on_command_error', author=ctx.author.mention,
                         infos=f"Commande utilisée: Inconnue\n\tErreur: {error}")

    #TODO
    """
    @commands.Cog.listener()
    async def on_command_error(self, ctx, error=None):
        log = f'@{ctx.author.display_name} in #{"DM" if isinstance(ctx.channel, discord.DMChannel) else ctx.channel.name}: {ctx.message.content}\n   -> {error}'
        if ctx.channel.id != ROLLS and not isinstance(ctx.channel, discord.DMChannel):
            chan = self.bot.get_channel(ROLLS)
            try:
                await ctx.message.delete()
            except:
                pass
        else:
            chan = ctx.channel
            await ctx.message.add_reaction(u"\u274C")
        if isinstance(error, commands.BadArgument):
            await chan.send(f'{ctx.author.mention}: **{ctx.message.content}**\nErreur: {error}')
            ERRlogger.warning(log)
        elif isinstance(error, commands.NoPrivateMessage):
            await chan.send(f"La commande `{ctx.prefix}{ctx.command}` ne s'utilise pas en MP.")
            ERRlogger.warning(log)
        elif isinstance(error, commands.CheckAnyFailure):
            if error.checks[0].__name__ == 'botAdmin':
                msg = await no_perms(ctx, chan, f'{ctx.author.mention}: **{ctx.message.content}**\nErreur: '
                                                f'Seul les ~~Dieux~~ DEV peuvent faire ceci !')
                await post_error(ctx, msg, 'User tried an admin command but is not on WhiteList.', self.bot.BotOwner)
                ERRlogger.critical(log)
            elif error.checks[0].__name__ == 'hasPerms':
                await no_perms(ctx, chan, f"{ctx.author.mention}: **{ctx.message.content}**\nErreur: "
                                          f"Vous n'avez pas le droit d'utiliser des commandes.")
                ERRlogger.warning(log)
            elif error.checks[0].__name__ == 'mod':
                await no_perms(ctx, chan, f'{ctx.author.mention}: **{ctx.message.content}**\nErreur: '
                                          f'Il faut être **Modérateur** pour utiliser cette commande.')
                ERRlogger.error(log)
            elif error.checks[0].__name__ == 'mj':
                await no_perms(ctx, chan, f'{ctx.author.mention}: **{ctx.message.content}**\nErreur: '
                                          f'Il faut être **MJ** pour utiliser cette commande.')
                ERRlogger.error(log)
            else:
                await no_perms(ctx, chan, f"{ctx.author.mention}: **{ctx.message.content}**\nErreur: "
                                          f"Vous n'avez pas les droits pour utiliser cette commande.")
                ERRlogger.error(log)
        elif isinstance(error, commands.CommandNotFound):
            try:
                await ctx.message.add_reaction(u"\u2049")
            except discord.errors.NotFound:
                pass
            await chan.send(f"{ctx.author.mention}: **{ctx.message.content}**\nErreur: Cette commande n'existe pas.")
            ERRlogger.warning(log)
        elif isinstance(error, commands.DisabledCommand):
            msg = await chan.send(f'{ctx.author.mention}: **{ctx.message.content}**\nErreur: Cette commande est '
                                  f'actuellement désactivée.')
            await post_error(ctx, msg, error, self.bot.BotOwner)
            ERRlogger.error(log)
        else:
            ERRlogger.critical(log)
            raise error
            
    """

def setup(bot):
    bot.add_cog(EventLoggerCog(bot))
