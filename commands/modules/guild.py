from discord.ext import commands
import discord, common, logging

class GuildCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.logger = common.get_newfilelogger(self.bot.options['LOGGERS']['guild_logname'],
                                               self.bot.options['LOGGERS']['guild_filename'],
                                               level=logging.INFO,
                                               console=True)
        self.logger.info("Instanciation du Cog 'GuildCog'")

    def cog_unload(self):
        self.logger.info("Fermeture du Cog 'GuildCog'")
        common.close_logger(self.logger)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        await member.guild.system_channel.send(f"{member.mention}, bienvenue sur {member.guild.name}!")
        common.log_event(self.logger, 'on_member_join',
                         infos=f"Member: {member}\n\tGuild: {member.guild}")

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        await member.guild.system_channel.send(f"Au revoir {member.name}!")
        common.log_event(self.logger, 'on_member_remove',
                         infos=f"Member: {member}\n\tGuild: {member.guild}")

    @commands.command(name='show_aar', hidden=True)
    async def show_autoassignableroles(self, ctx):
        result = "Roles auto-assignables:"
        for role in ctx.guild.roles:
            if role.name.endswith('_aar'):
                result += f"\n\t- {role.name}"
        await ctx.channel.send(result)

    @commands.guild_only()
    @commands.command(name='userlist', hidden=False)
    async def list(self, ctx, role: discord.Role):
        """Liste les membres possédant un role.
        Aliases:
            /userlist

        Parametres:
            role: Nom/Mention/ID d'un role.

        Exemple:
            /userlist MJ
        """
        if role.name == '@everyone':
            ctx.channel.send('Ce role contient tout le monde et ne se liste donc pas.')
            common.log_command_error(self.logger, ctx, 'Ce role contient tout le monde et ne se liste donc pas.')
            raise commands.BadArgument('Ce role contient tout le monde et ne se liste donc pas.')
        members = ''
        for member in role.members:
            members += member.mention + '\n'
        e = discord.Embed(title='Role Listing', colour=0x0DECAF)
        e.description = f'Voici tout les membres ayant le role {role.mention}:\n{members}'
        await ctx.send(embed=e)
        common.log_command(self.logger, ctx, f'Voici tout les membres ayant le role {role.mention}:\n\t{members}')

def setup(bot):
    bot.add_cog(GuildCog(bot))